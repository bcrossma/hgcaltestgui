# General Info

This directory contains a small example of how you can use the Zebra DS4608-SR scanner for reading HGCAL barcodes. 

# Requirements

Running the scanner requires the Zebra Scanner SDK which can be found at the following links:
- [SDK Download for Windows]{https://developer.zebra.com/products/barcode-scanners/windows-scanner-sdk}
- [SDK Download for Linux]{https://developer.zebra.com/products/barcode-scanners/linux-scanner-sdk}

Navigate to the "Download" button, click the "Downloads" drop down, and find the appropriate "CORESCANNER AND DEVEL INSTALLER PACKAGES" for your system.
Install the package using your package manager (Linux) or using the download manager (Windows).

N.B.: This code has not been tested on Windows. If available, use a Linux machine for running the scanner.

# Running the Zebra DS4608-SR Scanner

Clone the this repository and navigate to the Scanner directory:

    git clone ssh://git@gitlab.cern.ch:7999/bcrossma/hgcaltestgui.git
    cd hgcaltestgui/Scanner


The main scanner functionality is written in C++, so you will need to build the `runScanner` binary file.

    # In hgcaltestgui/Scanner
    make

This will create the binary in the `bin` directory. 
Now use the python wrapper of this binary to scan your barcode and decode it:

    cd python
    python get_barcodes.py

Try scanning any barcode. You should see the output of that barcode printed to the screen. 
The `run_scanner` function can be integrated into your workflow in any spot where a barcode scan is needed.

