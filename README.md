# HGCALTestGUI



## Getting started

This repository contains the code for running the HGCAL testing user interface. This software is intended to be used for HGCAL electronics testing.

Note that the GUI is still currently under development and will be ready for beta testing at other institutions soon.

## Installation

The use the following steps to clone the code base:
```
cd <your_working_directory>
git clone ssh://git@gitlab.cern.ch:7999/bcrossma/hgcaltestgui.git
```

## Test API

The Test API is used to integrate user developed tests into the GUI framework. The API class is intended to be inherited by all tests which need to be run by the GUI.

There are a few requirements for the current version of the API:

- The test class must be written in python for this version of the API
- The test class needs to take a connection option dictating where output is sent, use None to print to terminal
- Test are instantiated with arguments for a connection object responsible for test output (None, MP pipe, os Pipe), board id, tester name, and configuration file (yaml) path which dictates arguments for the test

## How to use the API

For a detailed look at how a test is implemented, see [the included example test](https://gitlab.cern.ch/bcrossma/hgcaltestgui/-/blob/master/TestAPI/demo_count.py?ref_type=heads).

Here is the step-by-step implementation procedure:
- Write your test class (in python) and ensure that the class has a single member function that performs the entire test (referenced to below as "run\_test")
- Inherit the Test API class as such:
```
from Test import Test

class my_test_class(Test):
...
```
- In the "\_\_init\_\_" function of your test class, you should create an instance of the test class:
```
    def __init__(self, conn, board_sn, tester, config_path):

        # Do some initialization...

        self.info_dict = {'name': <test_name>, 'board_sn': board_sn, 'tester': tester}

	# Pass test member function, info_dict, connection object, and configuration path into the parent function
	Test.__init__(self, self.run_test, self.info_dict, conn, config_path)

	# Start the test
	self.run()
```
- The "run\_test" function should take a single argument which is a dictionary containing all the configuration information for running the test
```
    def run_test(self, test_cfg):

        my_cfg_variable = test_cfg["my_cfg_variable"]
        # Use this variable to set up your test and run
```
- All print outs from the test for monitoring should be handled using the Test.send() function
```
       # Example print out
       self.send("Halfway done with the test")
```
- Ensure that your test is sending the expected results tuple back to the parent class
```
       # Test has finished
       test_passed = True
       data = {"foo": <some val>, "bar": {"bar1": <some val>, "bar2": <some val>}}
       result = "Test passed!"       

       return test_passed, result, data
```
- Write the yaml configuration file with all variables needed for running your test as well as the test path and test class
```
---
test_path: path/to/test/my_test.py
test_class: my_test_class

# Configuration variables below
cfg_var1: <some_val>
cfg_var2: <some_val>
...
```

To test your implementation, you can run:
```
python Test.py --config path/to/config.yaml
```

The test should run and the output of the test will be printed to the terminal.

## Questions/Comments

If there are any issues with test implementation, feel free to reach out directly (cros0400 (at) umn.edu) or open an issue here.
